package com.samad.authms.dto.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


public class SignupRequestDTO implements Serializable {

    @NotEmpty(message = "Firstname must be provided")
    private String firstName;

    @NotEmpty(message = "Lastname must be provided")
    private String lastName;

    private String patronymic;

    @Email(message = "Username should be email")
    private String username;

    @NotEmpty(message = "Password must be provided")
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public SignupRequestDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public SignupRequestDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public SignupRequestDTO setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public SignupRequestDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public SignupRequestDTO setPassword(String password) {
        this.password = password;
        return this;
    }
}
