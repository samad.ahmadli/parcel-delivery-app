package com.samad.authms.dto.response;

import java.io.Serializable;


public class SignInResponseDTO implements Serializable {

    private String jwtToken;

    public String getJwtToken() {
        return jwtToken;
    }

    public SignInResponseDTO setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
        return this;
    }
}
