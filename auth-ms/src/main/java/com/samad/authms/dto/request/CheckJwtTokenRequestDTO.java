package com.samad.authms.dto.request;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;


public class CheckJwtTokenRequestDTO implements Serializable {

    @NotEmpty(message = "Jwt token must be provided")
    private String jwtToken;

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
