package com.samad.authms.dto.request;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;


public class SignInRequestDTO implements Serializable {

    @NotEmpty(message = "Username must be provided")
    private String username;
    @NotEmpty(message = "Password must be provided")
    private String password;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
