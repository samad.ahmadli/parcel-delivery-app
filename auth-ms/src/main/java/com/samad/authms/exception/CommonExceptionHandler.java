package com.samad.authms.exception;

import com.samad.authms.exception.response.CustomFieldErrorResponse;
import com.samad.authms.exception.response.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;


@RestControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> authenticationExceptionHandler(Exception e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ErrorResponse.instance(401, "You dont have enough access for use this resource"));
    }

    @ExceptionHandler(CommonException.class)
    public ResponseEntity<?> commonExceptionHandler(CommonException ex) {
        ex.printStackTrace();
        return ResponseEntity.internalServerError().body(ErrorResponse.instance(ex.getCode(), ex.getMessage()));
    }


    @ExceptionHandler(CustomUserException.class)
    public ResponseEntity<?> customUserExceptionHandler(CustomUserException ex) {
        return ResponseEntity.badRequest().body(ErrorResponse.instance(ex.getCode(), ex.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exceptionHandler(Exception e) {
        e.printStackTrace();
        return ResponseEntity.internalServerError().body(ErrorResponse.instance(500, "Internal error occured"));
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<CustomFieldErrorResponse> customerFieldErrors = new ArrayList<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            customerFieldErrors.add(
                    new CustomFieldErrorResponse()
                            .setField(fieldError.getField())
                            .setMessage(fieldError.getDefaultMessage()));
        }
        return ResponseEntity.badRequest().body(ErrorResponse.instance(400, "Argument not valid", customerFieldErrors));
    }
}
