package com.samad.authms.exception;

public class CustomUserException extends RuntimeException {

    private Integer code;
    private String message;

    public CustomUserException(Integer code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
