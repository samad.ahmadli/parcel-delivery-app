package com.samad.authms.exception;

public class CommonException extends RuntimeException {

    private Integer code;
    private String message;

    public CommonException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
