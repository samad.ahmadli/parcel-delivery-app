package com.samad.authms.mapper;

import com.samad.authms.dto.response.CourierResponseDTO;
import com.samad.authms.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    CourierResponseDTO toDto(Users user);
}
