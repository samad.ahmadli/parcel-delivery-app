package com.samad.authms.controller;

import com.samad.authms.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/users")
@Api(value = "Services for users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/couriers")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(value = "Get all courier service")
    public ResponseEntity findAllCouriers() {
        return ResponseEntity.ok(userService.findAllCouriers());
    }
}
