package com.samad.authms.controller;

import com.samad.authms.dto.request.CheckJwtTokenRequestDTO;
import com.samad.authms.dto.request.SignInRequestDTO;
import com.samad.authms.dto.request.SignupRequestDTO;
import com.samad.authms.enums.UserTypes;
import com.samad.authms.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/auth")
@Api(value = "Auth services")
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }


    @PostMapping("/signup")
    @ApiOperation(value = "Sign up service for user")
    public ResponseEntity signUp(@Valid @RequestBody SignupRequestDTO signupRequest) {
        authService.signUpUser(signupRequest, UserTypes.USER);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/signup/courier")
    @ApiOperation(value = "Sign up service for courier")
    @PreAuthorize(value = "hasAnyRole('ADMIN')")
    public ResponseEntity signUpCourier(@Valid @RequestBody SignupRequestDTO signupRequest) {
        authService.signUpUser(signupRequest, UserTypes.COURIER);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/signin")
    @ApiOperation(value = "Sign in service for user")
    public ResponseEntity signIn(@Valid @RequestBody SignInRequestDTO signinRequest) {
        return ResponseEntity.ok(authService.signIn(signinRequest));
    }

    @PostMapping("/check/jwtToken")
    @ApiOperation(value = "Validate token service used for user roles and permissions, call from other microservices")
    public ResponseEntity checkJwtToken(@Valid @RequestBody CheckJwtTokenRequestDTO checkJwtRequest) {
        return ResponseEntity.ok(authService.validateJwtToken(checkJwtRequest));
    }
}
