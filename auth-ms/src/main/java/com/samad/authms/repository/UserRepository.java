package com.samad.authms.repository;

import com.samad.authms.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    Boolean existsByUsernameAndActive(String username, boolean isActive);

    @Query("select u from Users u " +
            "left join fetch u.roles r " +
            "where u.username = :username and u.active = true")
    Users getUserAndRolesByUsername(@Param(value = "username") String username);


    @Query(value = "select * from users u " +
            "inner join usersroles ur on ur.user_id = u.id " +
            "inner join roles r on r.id = ur.role_id where r.name = :roleName and u.active = true", nativeQuery = true)
    List<Users> findUsersByRoleName(@Param(value = "roleName") String roleName);
}
