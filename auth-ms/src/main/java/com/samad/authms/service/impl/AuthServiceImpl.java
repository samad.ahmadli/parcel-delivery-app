package com.samad.authms.service.impl;

import com.samad.authms.dto.request.CheckJwtTokenRequestDTO;
import com.samad.authms.dto.request.SignInRequestDTO;
import com.samad.authms.dto.request.SignupRequestDTO;
import com.samad.authms.dto.response.CheckJwtTokenResponse;
import com.samad.authms.dto.response.SignInResponseDTO;
import com.samad.authms.entity.Roles;
import com.samad.authms.entity.UserRoles;
import com.samad.authms.entity.Users;
import com.samad.authms.enums.UserTypes;
import com.samad.authms.exception.CommonException;
import com.samad.authms.exception.CustomUserException;
import com.samad.authms.security.JwtTokenProvider;
import com.samad.authms.service.AuthService;
import com.samad.authms.service.RoleService;
import com.samad.authms.service.UserRoleService;
import com.samad.authms.service.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class AuthServiceImpl implements AuthService {
    private final UserService userService;
    private final RoleService roleService;
    private final UserRoleService userRoleService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    public AuthServiceImpl(UserService userService, RoleService roleService, UserRoleService userRoleService, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.roleService = roleService;
        this.userRoleService = userRoleService;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
    }


    @Override
    public void signUpUser(SignupRequestDTO signupRequest, UserTypes userType) {

        if (userService.checkUserAlreadyExists(signupRequest.getUsername())) {
            throw new CustomUserException(409, "Username already exists - " + signupRequest.getUsername());
        }

        Roles role = roleService.findRoleByName(UserTypes.getRoleValue(userType));
        if (role == null) {
            throw new CommonException(500, "Role not found - " + userType);
        }

        Users newUser = new Users()
                .setFirstName(signupRequest.getFirstName())
                .setLastName(signupRequest.getLastName())
                .setPatronymic(signupRequest.getPatronymic())
                .setUsername(signupRequest.getUsername())
                .setPassword(passwordEncoder.encode(signupRequest.getPassword()));
        newUser = userService.saveUser(newUser);

        UserRoles newUserRole = new UserRoles()
                .setRole(role).setUser(newUser);
        userRoleService.save(newUserRole);
    }

    @Override
    public SignInResponseDTO signIn(SignInRequestDTO signInRequest) {

        Users user = userService.findUserByUsername(signInRequest.getUsername());
        if (user == null) {
            throw new CustomUserException(404, "Username is incorrect");
        }

        if (!passwordEncoder.matches(signInRequest.getPassword(), user.getPassword())) {
            throw new CustomUserException(400, "Password is incorrect");
        }

        //authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword()));
        final String jwtToken = jwtTokenProvider.generateToken(signInRequest.getUsername());
        return new SignInResponseDTO().setJwtToken(jwtToken);
    }

    @Override
    public CheckJwtTokenResponse validateJwtToken(CheckJwtTokenRequestDTO checkJwtTokenRequest) {

        String jwtToken = checkJwtTokenRequest.getJwtToken();
        if (jwtTokenProvider.validateToken(jwtToken)) {

            String username = jwtTokenProvider.getUsernameFromJWT(jwtToken);
            Users user = userService.findUserByUsername(username);
            return CheckJwtTokenResponse.success
                    (
                            user.getUsername(), user.getFirstName(), user.getLastName(),
                            user.getRoles().stream().map(roles -> UserTypes.getRoleNameByValue(roles.getName())).collect(Collectors.toList())
                    );
        }

        return CheckJwtTokenResponse.fail();
    }

}
