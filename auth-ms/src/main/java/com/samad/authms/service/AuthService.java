package com.samad.authms.service;

import com.samad.authms.dto.request.CheckJwtTokenRequestDTO;
import com.samad.authms.dto.request.SignInRequestDTO;
import com.samad.authms.dto.request.SignupRequestDTO;
import com.samad.authms.dto.response.SignInResponseDTO;
import com.samad.authms.dto.response.CheckJwtTokenResponse;
import com.samad.authms.enums.UserTypes;


public interface AuthService {

    void signUpUser(SignupRequestDTO signupRequest, UserTypes userType);

    SignInResponseDTO signIn(SignInRequestDTO signInRequest);

    CheckJwtTokenResponse validateJwtToken(CheckJwtTokenRequestDTO checkJwtTokenRequest);
}
