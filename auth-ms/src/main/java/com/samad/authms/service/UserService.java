package com.samad.authms.service;

import com.samad.authms.dto.response.CourierResponseDTO;
import com.samad.authms.entity.Users;

import java.util.List;


public interface UserService {

    Boolean checkUserAlreadyExists(String username);

    Users saveUser(Users newUser);

    Users findUserByUsername(String username);

    List<CourierResponseDTO> findAllCouriers();
}
