package com.samad.authms.service;

import com.samad.authms.entity.Roles;


public interface RoleService {

    Roles findRoleByName(String roleName);
}
