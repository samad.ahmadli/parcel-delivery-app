package com.samad.authms.service.impl;

import com.samad.authms.dto.response.CourierResponseDTO;
import com.samad.authms.entity.Users;
import com.samad.authms.enums.UserTypes;
import com.samad.authms.mapper.UserMapper;
import com.samad.authms.repository.UserRepository;
import com.samad.authms.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Boolean checkUserAlreadyExists(String username) {
        return userRepository.existsByUsernameAndActive(username, true);
    }

    @Override
    @Transactional
    public Users saveUser(Users newUser) {
        return userRepository.save(newUser);
    }

    @Override
    public Users findUserByUsername(String username) {
        return userRepository.getUserAndRolesByUsername(username);
    }


    @Override
    public List<CourierResponseDTO> findAllCouriers() {
        return userRepository.findUsersByRoleName(UserTypes.getRoleValue(UserTypes.COURIER))
                .stream().map(UserMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

}
