package com.samad.authms.service;

import com.samad.authms.entity.UserRoles;


public interface UserRoleService {

    UserRoles save(UserRoles newUserRole);

}
