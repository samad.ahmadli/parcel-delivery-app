package com.samad.authms.service.impl;

import com.samad.authms.entity.Roles;
import com.samad.authms.repository.RoleRepository;
import com.samad.authms.service.RoleService;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }


    @Override
    public Roles findRoleByName(String roleName) {
        return roleRepository.findByName(roleName);
    }
}
