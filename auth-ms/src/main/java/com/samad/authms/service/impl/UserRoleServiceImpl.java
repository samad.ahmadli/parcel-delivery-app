package com.samad.authms.service.impl;

import com.samad.authms.entity.UserRoles;
import com.samad.authms.repository.UserRoleRepository;
import com.samad.authms.service.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {
    private final UserRoleRepository userRoleRepository;

    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }


    @Override
    public UserRoles save(UserRoles newUserRole) {
        return userRoleRepository.save(newUserRole);
    }
}
