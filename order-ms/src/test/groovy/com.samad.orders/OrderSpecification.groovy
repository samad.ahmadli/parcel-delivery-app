package com.samad.orders

import com.samad.orderms.config.AuthUserBean
import com.samad.orderms.dto.request.AssignCourierRequestDTO
import com.samad.orderms.dto.request.ChangeDestinationRequestDTO
import com.samad.orderms.dto.request.CreateOrderRequestDTO
import com.samad.orderms.dto.response.OrderResponseDTO
import com.samad.orderms.entity.Orders
import com.samad.orderms.enums.OrderStatuses
import com.samad.orderms.messagebroker.publish.ParcelPublisher
import com.samad.orderms.repository.OrderRepository
import com.samad.orderms.service.impl.OrderServiceImpl
import spock.lang.Specification


class OrderSpecification extends Specification {

    ParcelPublisher parcelPublisher = Mock()
    OrderRepository orderRepository = Mock()
    AuthUserBean authUserBean = Mock()
    OrderServiceImpl orderService = Mock()

    def setup() {
        orderService = new OrderServiceImpl(parcelPublisher, orderRepository, authUserBean)
    }

    def "two plus two should equal four"() {
        given:
        int left = 2
        int right = 2

        when:
        int result = left + right

        then:
        result == 4
    }

    def "createNewOrder() should be successful"() {

        given:
        def requestDto = new CreateOrderRequestDTO().setName("My first order").setDestination("Ozbekistan 1").setWeight(2.2)
        def orderResponse = new Orders().setName("My first order").setDestination("Ozbekistan 1").setStatus(OrderStatuses.CREATED)

        when:
        orderService.createNewOrder(requestDto)

        then:
        1 * orderRepository.save(_) >> orderResponse
    }

    def "changeDestination() should be successful"() {
        given:
        def requestDTO = new ChangeDestinationRequestDTO().setOrderId(1).setDestination("New destination for change")

        def order = new Orders().setId(1).setStatus(OrderStatuses.CREATED)

        def responseOrder = new Orders().setId(1).setDestination("New destination for change")

        when:
        orderService.changeDestination(requestDTO)

        then:
        1 * orderRepository.getOrdersByIdAndCustomerUsername(*_) >> Optional.of(order)
        1 * orderRepository.save(*_) >> responseOrder
    }

    def "cancelOrder() should be successful"() {
        given:
        def orderId = 1
        def order = new Orders().setId(orderId).setStatus(OrderStatuses.CREATED)
        def responseOrder = new Orders().setId(orderId).setStatus(OrderStatuses.CANCELED)

        when:
        orderService.cancelOrder(orderId)

        then:
        1 * authUserBean.getRoles() >> Arrays.asList("USER")
        1 * orderRepository.getOrdersByIdAndCustomerUsername(*_) >> Optional.of(order)
        1 * orderRepository.save(_) >> responseOrder
    }

    def "assignCourierForOrder() shoud be successful"() {
        given:
        def assignCourierRequest = new AssignCourierRequestDTO().setOrderId(1).setCourierUsername("courier@gmail.com")
        def order = new Orders().setId(1).setStatus(OrderStatuses.CREATED)

        when:
        orderService.assignCourierForOrder(assignCourierRequest)

        then:
        1 * orderRepository.getOrdersById(_) >> Optional.of(order)
        1 * orderRepository.save(_) >> order
        1 * parcelPublisher.publishParcel(_)
    }

    def "getOrderDetailById() shoud be successful"() {
        given:
        def orderId = 1
        def order = new Orders().setId(orderId).setName("My first order").setStatus(OrderStatuses.ASSIGNED_COURIER)

        when:
        def orderResponse = orderService.getOrderDetailById(orderId)

        then:
        1 * authUserBean.getRoles() >> Arrays.asList("USER")
        1 * orderRepository.getOrdersByIdAndCustomerUsername(*_) >> Optional.of(order)
        orderResponse.getName() == "My first order"
        orderResponse.getStatus() == OrderStatuses.ASSIGNED_COURIER
    }

    def "getAllOrders() should be successful"() {
        given:
        def order = new Orders().setName("My first order").setStatus(OrderStatuses.ASSIGNED_COURIER)

        when:
        def orders = orderService.getAllOrders()

        then:
        1 * authUserBean.getRoles() >> Arrays.asList("ADMIN")
        1 * orderRepository.findAll() >> Arrays.asList(order)
        orders.size() == 1
    }
}
