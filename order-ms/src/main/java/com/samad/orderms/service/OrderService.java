package com.samad.orderms.service;

import com.samad.orderms.dto.request.ChangeDestinationRequestDTO;
import com.samad.orderms.dto.request.CreateOrderRequestDTO;
import com.samad.orderms.dto.request.AssignCourierRequestDTO;
import com.samad.orderms.dto.response.OrderResponseDTO;
import com.samad.orderms.entity.Orders;
import com.samad.orderms.messagebroker.model.OrderConsumerModel;

import java.util.List;
import java.util.Optional;


public interface OrderService {

    Orders save(Orders order);

    void createNewOrder(CreateOrderRequestDTO createOrderRequest);

    void changeDestination(ChangeDestinationRequestDTO changeDestinationRequest);

    void cancelOrder(Long orderId);

    void assignCourierForOrder(AssignCourierRequestDTO assignToCourierRequest);

    OrderResponseDTO getOrderDetailById(Long orderId);

    List<OrderResponseDTO> getAllOrders();

    Optional<Orders> getOrderById(Long orderId);

    Optional<Orders> getOrderByIdAndCustomerUsername(Long orderId, String customerUsername);

    void changeOrderStatus(OrderConsumerModel orderConsumerModel);
}
