package com.samad.orderms.service.impl;

import com.samad.orderms.config.AuthUserBean;
import com.samad.orderms.dto.request.CheckJwtTokenRequestDTO;
import com.samad.orderms.dto.response.CheckJwtTokenResponse;
import com.samad.orderms.service.AuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;


@Service("authService")
public class AuthServiceImpl implements AuthService {

    @Value("${authms.ckeck.jwtToken.url}")
    private String authCheckJwtUrl;

    private final AuthUserBean authUserBean;
    private final RestTemplate restTemplate;

    public AuthServiceImpl(AuthUserBean authUserBean, RestTemplate restTemplate) {
        this.authUserBean = authUserBean;
        this.restTemplate = restTemplate;
    }


    @Override
    public boolean checkUserRoles(String jwtToken, String... possibleRoles) {

        CheckJwtTokenResponse checkTokenResponse = restTemplate.postForObject
                (authCheckJwtUrl, new CheckJwtTokenRequestDTO().setJwtToken(jwtToken), CheckJwtTokenResponse.class);

        if (checkTokenResponse.isSuccess() &&
                checkTokenResponse.getRoles().stream().anyMatch(
                        userRole -> Arrays.asList(possibleRoles).contains(userRole))) {

            authUserBean.setUsername(checkTokenResponse.getUsername())
                    .setFirstName(checkTokenResponse.getFirstName())
                    .setLastName(checkTokenResponse.getLastName())
                    .setRoles(checkTokenResponse.getRoles());
            return true;
        }

        return false;
    }
}
