package com.samad.orderms.service.impl;

import com.samad.orderms.config.AuthUserBean;
import com.samad.orderms.dto.request.AssignCourierRequestDTO;
import com.samad.orderms.dto.request.ChangeDestinationRequestDTO;
import com.samad.orderms.dto.request.CreateOrderRequestDTO;
import com.samad.orderms.dto.response.OrderResponseDTO;
import com.samad.orderms.entity.Orders;
import com.samad.orderms.enums.OrderStatuses;
import com.samad.orderms.enums.UserTypes;
import com.samad.orderms.exception.OrderExceptions;
import com.samad.orderms.helper.OrderHelper;
import com.samad.orderms.mapper.OrderMapper;
import com.samad.orderms.messagebroker.model.OrderConsumerModel;
import com.samad.orderms.messagebroker.model.ParcelPublisherModel;
import com.samad.orderms.messagebroker.publish.ParcelPublisher;
import com.samad.orderms.repository.OrderRepository;
import com.samad.orderms.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    private final ParcelPublisher parcelPublisher;
    private final OrderRepository orderRepository;
    private final AuthUserBean authUserBean;

    public OrderServiceImpl(ParcelPublisher parcelPublisher, OrderRepository orderRepository, AuthUserBean authUserBean) {
        this.parcelPublisher = parcelPublisher;
        this.orderRepository = orderRepository;
        this.authUserBean = authUserBean;
    }


    @Override
    public Orders save(Orders order) {
        return orderRepository.save(order);
    }

    @Override
    public void createNewOrder(CreateOrderRequestDTO createOrderRequest) {

        Orders newOrder = new Orders()
                .setStatus(OrderStatuses.CREATED)
                .setName(createOrderRequest.getName())
                .setWeight(createOrderRequest.getWeight())
                .setCustomerUsername(authUserBean.getUsername())
                .setDescription(createOrderRequest.getDescription())
                .setDestination(createOrderRequest.getDestination())
                .setPrice(OrderHelper.calculatePrice(createOrderRequest.getWeight()));
        orderRepository.save(newOrder);
    }

    @Override
    public void changeDestination(ChangeDestinationRequestDTO changeDestinationRequest) {

        Orders order = orderRepository.getOrdersByIdAndCustomerUsername(changeDestinationRequest.getOrderId(), authUserBean.getUsername())
                .orElseThrow(() -> new OrderExceptions(400, "Order not found - order_id:" + changeDestinationRequest.getOrderId()));

        if (Arrays.asList(OrderStatuses.CANCELED, OrderStatuses.DELIVERED).contains(order.getStatus())) {
            throw new OrderExceptions(400, "You can not change destination of order - order_id:" + changeDestinationRequest.getOrderId());
        }

        order.setUpdatedAt(LocalDateTime.now());
        order.setDestination(changeDestinationRequest.getDestination());
        save(order);
    }

    @Override
    public void cancelOrder(Long orderId) {

        Optional<Orders> optional = authUserBean.getRoles().contains(UserTypes.ADMIN.name()) ?
                getOrderById(orderId) :
                getOrderByIdAndCustomerUsername(orderId, authUserBean.getUsername());
        Orders order = optional.orElseThrow(() -> new OrderExceptions(400, "Order not found - order_id:" + orderId));

        if (Arrays.asList(OrderStatuses.CANCELED, OrderStatuses.DELIVERED).contains(order.getStatus())) {
            throw new OrderExceptions(400, "You can not change destination of order - order_id:" + orderId);
        }

        OrderStatuses orderOldStatus = order.getStatus();
        order = save(order.setStatus(OrderStatuses.CANCELED).setUpdatedAt(LocalDateTime.now()));

        if (Arrays.asList(OrderStatuses.ASSIGNED_COURIER, OrderStatuses.IN_PROGRESS).contains(orderOldStatus)) {
            publishParcelToBroker(order);
        }
    }

    @Override
    public void assignCourierForOrder(AssignCourierRequestDTO assignToCourierRequest) {

        Orders order = orderRepository.getOrdersById(assignToCourierRequest.getOrderId())
                .orElseThrow(() -> new OrderExceptions(400, "Order not found - orderId:" + assignToCourierRequest.getOrderId()));

        if (!order.getStatus().equals(OrderStatuses.CREATED)) {
            throw new OrderExceptions(400, "You can not assign courier for this order - orderStatus:" + order.getStatus());
        }

        order.setStatus(OrderStatuses.ASSIGNED_COURIER).setCourierUsername(assignToCourierRequest.getCourierUsername());
        order = save(order);
        publishParcelToBroker(order);
    }

    @Override
    @Transactional(readOnly = true)
    public OrderResponseDTO getOrderDetailById(Long orderId) {
        Optional<Orders> optional = authUserBean.getRoles().contains(UserTypes.ADMIN.name()) ?
                getOrderById(orderId) :
                getOrderByIdAndCustomerUsername(orderId, authUserBean.getUsername());
        Orders order = optional.orElseThrow(() -> new OrderExceptions(400, "Order not found - order_id:" + orderId));
        return OrderMapper.INSTANCE.toDto(order);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderResponseDTO> getAllOrders() {
        List<Orders> allOrders = authUserBean.getRoles().contains(UserTypes.ADMIN.name()) ?
                orderRepository.findAll() :
                orderRepository.getAllByCustomerUsername(authUserBean.getUsername());
        return allOrders.parallelStream().map(OrderMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Orders> getOrderById(Long orderId) {
        return orderRepository.getOrdersById(orderId);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Orders> getOrderByIdAndCustomerUsername(Long orderId, String customerUsername) {
        return orderRepository.getOrdersByIdAndCustomerUsername(orderId, customerUsername);
    }

    @Override
    public void changeOrderStatus(OrderConsumerModel orderConsumerModel) {

        Orders order = getOrderById(orderConsumerModel.getOrderId())
                .orElseThrow(() -> new OrderExceptions(400, "Order not found - order_id:" + orderConsumerModel.getOrderId()));

        if (order.getStatus().equals(OrderStatuses.ASSIGNED_COURIER) && orderConsumerModel.getStatus().equals(OrderStatuses.IN_PROGRESS)) {
            order.setStatus(OrderStatuses.IN_PROGRESS);
        } else if (order.getStatus().equals(OrderStatuses.IN_PROGRESS) && orderConsumerModel.getStatus().equals(OrderStatuses.DELIVERED)) {
            order.setStatus(OrderStatuses.DELIVERED);
        }

        save(order.setUpdatedAt(LocalDateTime.now()));
    }

    private void publishParcelToBroker(Orders order) {
        ParcelPublisherModel publisherModel =
                new ParcelPublisherModel()
                        .setOrderId(order.getId())
                        .setPrice(order.getPrice())
                        .setWeight(order.getWeight())
                        .setStatus(order.getStatus())
                        .setOrderName(order.getName())
                        .setDestination(order.getDestination())
                        .setDescription(order.getDescription())
                        .setCourierUsername(order.getCourierUsername())
                        .setCustomerUsername(order.getCustomerUsername());
        parcelPublisher.publishParcel(publisherModel);
    }
}
