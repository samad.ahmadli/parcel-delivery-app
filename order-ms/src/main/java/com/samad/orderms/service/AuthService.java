package com.samad.orderms.service;

public interface AuthService {

    boolean checkUserRoles(String jwtToken, String[] possibleRoles);
}
