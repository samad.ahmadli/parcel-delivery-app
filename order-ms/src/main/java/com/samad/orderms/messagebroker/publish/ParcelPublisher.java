package com.samad.orderms.messagebroker.publish;

import com.samad.orderms.config.RabbitMQConfig;
import com.samad.orderms.messagebroker.model.ParcelPublisherModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;


@Component
public class ParcelPublisher {

    private final Logger log = LoggerFactory.getLogger(ParcelPublisher.class);
    private final RabbitTemplate rabbitTemplate;

    public ParcelPublisher(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    public void publishParcel(ParcelPublisherModel parcelPublisherModel) {

        log.debug("Parcel publish event is started, {} " + parcelPublisherModel);
        rabbitTemplate.convertAndSend(RabbitMQConfig.PARCEL_QUEUE, parcelPublisherModel);
        log.debug("Parcel publish event is ended, {} " + parcelPublisherModel);
    }
}
