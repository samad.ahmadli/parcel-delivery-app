package com.samad.orderms.messagebroker.model;

import com.samad.orderms.enums.OrderStatuses;


public class OrderConsumerModel {

    private Long orderId;
    private OrderStatuses status;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public OrderStatuses getStatus() {
        return status;
    }

    public void setStatus(OrderStatuses status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderConsumerModel{" +
                "orderId=" + orderId +
                ", status=" + status +
                '}';
    }
}
