package com.samad.orderms.messagebroker.consume;

import com.samad.orderms.config.RabbitMQConfig;
import com.samad.orderms.messagebroker.model.OrderConsumerModel;
import com.samad.orderms.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


@Component
public class OrderStatusConsumer {
    private static final Logger log = LoggerFactory.getLogger(OrderStatusConsumer.class);

    private final OrderService orderService;

    public OrderStatusConsumer(OrderService orderService) {
        this.orderService = orderService;
    }


    @RabbitListener(queues = RabbitMQConfig.ORDER_QUEUE)
    public void receiveOrderStatusFromQueue(OrderConsumerModel consumerModel) {

        log.debug("Order status consumed {}" + consumerModel);
        orderService.changeOrderStatus(consumerModel);
        log.debug("Order consumed process finished {}" + consumerModel);
    }
}
