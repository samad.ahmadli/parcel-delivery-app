package com.samad.orderms.messagebroker.model;

import com.samad.orderms.enums.OrderStatuses;

import java.math.BigDecimal;


public class ParcelPublisherModel {

    private Long orderId;
    private String orderName;
    private String destination;
    private String description;
    private Double weight;
    private BigDecimal price;
    private OrderStatuses status;
    private String customerUsername;
    private String courierUsername;


    public Long getOrderId() {
        return orderId;
    }

    public ParcelPublisherModel setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getOrderName() {
        return orderName;
    }

    public ParcelPublisherModel setOrderName(String orderName) {
        this.orderName = orderName;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public ParcelPublisherModel setDestination(String destination) {
        this.destination = destination;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ParcelPublisherModel setDescription(String description) {
        this.description = description;
        return this;
    }

    public Double getWeight() {
        return weight;
    }

    public ParcelPublisherModel setWeight(Double weight) {
        this.weight = weight;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public ParcelPublisherModel setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public OrderStatuses getStatus() {
        return status;
    }

    public ParcelPublisherModel setStatus(OrderStatuses status) {
        this.status = status;
        return this;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public ParcelPublisherModel setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
        return this;
    }

    public String getCourierUsername() {
        return courierUsername;
    }

    public ParcelPublisherModel setCourierUsername(String courierUsername) {
        this.courierUsername = courierUsername;
        return this;
    }

    @Override
    public String toString() {
        return "ParcelPublisherModel{" +
                "orderId=" + orderId +
                ", orderName='" + orderName + '\'' +
                ", destination='" + destination + '\'' +
                ", description='" + description + '\'' +
                ", weight=" + weight +
                ", price=" + price +
                ", status=" + status +
                ", customerUsername='" + customerUsername + '\'' +
                ", courierUsername='" + courierUsername + '\'' +
                '}';
    }
}
