package com.samad.orderms.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


public class AssignCourierRequestDTO implements Serializable {

    @NotNull(message = "Order id must be provided")
    private Long orderId;
    @NotEmpty(message = "Courier username must be provided")
    private String courierUsername;

    public Long getOrderId() {
        return orderId;
    }

    public AssignCourierRequestDTO setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getCourierUsername() {
        return courierUsername;
    }

    public AssignCourierRequestDTO setCourierUsername(String courierUsername) {
        this.courierUsername = courierUsername;
        return this;
    }
}
