package com.samad.orderms.dto.response;

import java.io.Serializable;
import java.util.List;


public class CheckJwtTokenResponse implements Serializable {

    private boolean success;
    private String username;
    private String firstName;
    private String lastName;
    private List<String> roles;


    public boolean isSuccess() {
        return success;
    }

    public CheckJwtTokenResponse setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public CheckJwtTokenResponse setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public CheckJwtTokenResponse setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public CheckJwtTokenResponse setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public List<String> getRoles() {
        return roles;
    }

    public CheckJwtTokenResponse setRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }
}
