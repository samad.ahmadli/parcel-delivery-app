package com.samad.orderms.dto.request;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;


public class CreateOrderRequestDTO implements Serializable {

    @NotEmpty(message = "Order name is required")
    private String name;
    private String description;

    private Double weight;
    // will calculate by wight
    private BigDecimal price;
    @NotEmpty(message = "Order destination is required")
    private String destination;

    public String getName() {
        return name;
    }

    public CreateOrderRequestDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CreateOrderRequestDTO setDescription(String description) {
        this.description = description;
        return this;
    }

    public Double getWeight() {
        return weight;
    }

    public CreateOrderRequestDTO setWeight(Double weight) {
        this.weight = weight;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public CreateOrderRequestDTO setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public CreateOrderRequestDTO setDestination(String destination) {
        this.destination = destination;
        return this;
    }
}
