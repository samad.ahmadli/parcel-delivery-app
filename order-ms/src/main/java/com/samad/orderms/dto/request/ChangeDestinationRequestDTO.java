package com.samad.orderms.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ChangeDestinationRequestDTO implements Serializable {

    @NotNull(message = "Order id must be provided")
    private Long orderId;
    @NotEmpty(message = "Order destination must be provided")
    private String destination;

    public Long getOrderId() {
        return orderId;
    }

    public ChangeDestinationRequestDTO setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public ChangeDestinationRequestDTO setDestination(String destination) {
        this.destination = destination;
        return this;
    }
}
