package com.samad.orderms.mapper;

import com.samad.orderms.dto.response.OrderResponseDTO;
import com.samad.orderms.entity.Orders;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderResponseDTO toDto(Orders order);
}
