package com.samad.orderms.enums;

public enum OrderStatuses {

    CREATED,
    ASSIGNED_COURIER,
    IN_PROGRESS,
    DELIVERED,
    CANCELED
}
