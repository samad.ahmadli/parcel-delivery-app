package com.samad.orderms.repository;

import com.samad.orderms.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface OrderRepository extends JpaRepository<Orders, Long> {

    Optional<Orders> getOrdersByIdAndCustomerUsername(Long id, String username);

    Optional<Orders> getOrdersById(Long id);

    List<Orders> getAllByCustomerUsername(String customerUsername);
}
