package com.samad.orderms.entity;

import com.samad.orderms.enums.OrderStatuses;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Entity
@Table(name = "orders")
@DynamicInsert
@DynamicUpdate
public class Orders implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private OrderStatuses status;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "customer_username", nullable = false)
    private String customerUsername;

    @Column(name = "courier_username")
    private String courierUsername;

    @Column(name = "destination", nullable = false)
    private String destination;

    @Basic(optional = false)
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;


    public Long getId() {
        return id;
    }

    public Orders setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Orders setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Orders setDescription(String description) {
        this.description = description;
        return this;
    }

    public OrderStatuses getStatus() {
        return status;
    }

    public Orders setStatus(OrderStatuses status) {
        this.status = status;
        return this;
    }

    public Double getWeight() {
        return weight;
    }

    public Orders setWeight(Double weight) {
        this.weight = weight;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Orders setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public Orders setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
        return this;
    }

    public String getCourierUsername() {
        return courierUsername;
    }

    public Orders setCourierUsername(String courierUsername) {
        this.courierUsername = courierUsername;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public Orders setDestination(String destination) {
        this.destination = destination;
        return this;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Orders setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Orders setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
}
