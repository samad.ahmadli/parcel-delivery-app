package com.samad.orderms.exception;

public class OrderExceptions extends RuntimeException{

    private Integer code;
    private String message;

    public OrderExceptions(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
