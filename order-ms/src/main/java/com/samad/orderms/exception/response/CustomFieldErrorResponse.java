package com.samad.orderms.exception.response;


public class CustomFieldErrorResponse {

    public String message;
    public String field;

    public CustomFieldErrorResponse() {
    }

    public CustomFieldErrorResponse(String message, String field) {
        this.message = message;
        this.field = field;
    }

    public CustomFieldErrorResponse(String code, String message, String field) {
        this.message = message;
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public CustomFieldErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getField() {
        return field;
    }

    public CustomFieldErrorResponse setField(String field) {
        this.field = field;
        return this;
    }
}
