package com.samad.orderms.exception.response;

import java.util.List;

public class ErrorResponse {

    private Integer code;
    private String message;
    private List<CustomFieldErrorResponse> fields;


    public ErrorResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ErrorResponse(Integer code, String message, List<CustomFieldErrorResponse> fields) {
        this.code = code;
        this.message = message;
        this.fields = fields;
    }

    public static ErrorResponse instance(Integer code, String message) {
        return new ErrorResponse(code, message);
    }

    public static ErrorResponse instance(Integer code, String message, List<CustomFieldErrorResponse> fields) {
        return new ErrorResponse(code, message, fields);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<CustomFieldErrorResponse> getFields() {
        return fields;
    }

    public void setFields(List<CustomFieldErrorResponse> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        return "Error: code: " + getCode() + " Message: " + getMessage();
    }
}
