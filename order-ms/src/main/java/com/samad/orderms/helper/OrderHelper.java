package com.samad.orderms.helper;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;


@Component
public class OrderHelper {

    public static BigDecimal calculatePrice(Double weight) {
        return BigDecimal.valueOf(weight * 2);
    }
}
