package com.samad.orderms.controller;

import com.samad.orderms.dto.request.ChangeDestinationRequestDTO;
import com.samad.orderms.dto.request.CreateOrderRequestDTO;
import com.samad.orderms.dto.request.AssignCourierRequestDTO;
import com.samad.orderms.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("/api/orders")
@Api(value = "Services for orders")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }


    @PostMapping("/create")
    @ApiOperation(value = "Create new order service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'USER')")
    public ResponseEntity<?> createNewOrder
            (
                    @Valid @RequestBody CreateOrderRequestDTO requestDto,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        orderService.createNewOrder(requestDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value = "Service for change destination")
    @PutMapping("/change/destination")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'USER')")
    public ResponseEntity<?> changeDestination
            (
                    @Valid @RequestBody ChangeDestinationRequestDTO changeDestinationRequest,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        orderService.changeDestination(changeDestinationRequest);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/cancel/{orderId}")
    @ApiOperation(value = "Cancel order service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'USER','ADMIN')")
    public ResponseEntity<?> cancelOrder
            (
                    @PathVariable(value = "orderId") @NotNull(message = "Order id must be provided") Long orderId,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        orderService.cancelOrder(orderId);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    @ApiOperation(value = "Get all orders service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'USER','ADMIN')")
    public ResponseEntity<?> getAllOrders
            (
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        return ResponseEntity.ok(orderService.getAllOrders());
    }


    @GetMapping("/{orderId}")
    @ApiOperation(value = "Get order details service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'USER','ADMIN')")
    public ResponseEntity<?> getOrderDetailsById
            (
                    @PathVariable(value = "orderId") @NotNull(message = "Order id must be provided") Long orderId,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        return ResponseEntity.ok(orderService.getOrderDetailById(orderId));
    }

    @PutMapping("/assign/courier")
    @ApiOperation(value = "Assign order to courier service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'ADMIN')")
    public ResponseEntity<?> assignCourier
            (
                    @Valid @RequestBody AssignCourierRequestDTO assignToCourier,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        orderService.assignCourierForOrder(assignToCourier);
        return ResponseEntity.ok().build();
    }

}
