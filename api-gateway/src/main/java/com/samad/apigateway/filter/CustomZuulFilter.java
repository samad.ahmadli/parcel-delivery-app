package com.samad.apigateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class CustomZuulFilter extends ZuulFilter {
    private final Logger logger = LoggerFactory.getLogger(CustomZuulFilter.class);

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader("zuul_test_header", "test_info");
        logger.debug("Request method: {}, Request Url: {} ", ctx.getRequest().getMethod(), ctx.getRequest().getRequestURI());
        return null;
    }

}
