package com.samad.parcelms.controller;

import com.samad.parcelms.dto.request.ChangeParcelStatusRequestDTO;
import com.samad.parcelms.service.ParcelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("/api/parcels")
@Api(value = "Services for parcels")
public class ParcelController {
    private final ParcelService parcelService;

    public ParcelController(ParcelService parcelService) {
        this.parcelService = parcelService;
    }


    @GetMapping
    @ApiOperation(value = "Get all parcels service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'COURIER','ADMIN')")
    public ResponseEntity<?> getAllParcels
            (
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        return ResponseEntity.ok(parcelService.getAllParcels());
    }


    @GetMapping("/{id}")
    @ApiOperation(value = "Get parcel details service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'COURIER','ADMIN')")
    public ResponseEntity<?> getParcelDetailsById
            (
                    @PathVariable("id") @NotNull(message = "Parcel id must be provided") Long parcelId,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        return ResponseEntity.ok(parcelService.getParcelDetailsById(parcelId));
    }


    @PutMapping("/change/status/{id}")
    @ApiOperation(value = "Change parcel status to in-progress or delivered by courier service")
    @PreAuthorize("@authService.checkUserRoles(#jwtToken,'COURIER')")
    public ResponseEntity<?> changeParcelStatus
            (
                    @Valid @RequestBody ChangeParcelStatusRequestDTO changeParcelStatusRequest,
                    @PathVariable("id") @NotNull(message = "Parcel id must be provided") Long parcelId,
                    @RequestHeader("Authorization") @NotBlank(message = "Jwt token is required") String jwtToken
            ) {
        parcelService.changeParcelStatus(changeParcelStatusRequest, parcelId);
        return ResponseEntity.ok().build();
    }

}
