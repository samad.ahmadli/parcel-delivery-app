package com.samad.parcelms.config;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AuthUserBean {

    private String username;
    private String firstName;
    private String lastName;
    private List<String> roles;

    public String getUsername() {
        return username;
    }

    public AuthUserBean setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public AuthUserBean setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public AuthUserBean setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public List<String> getRoles() {
        return roles;
    }

    public AuthUserBean setRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }
}
