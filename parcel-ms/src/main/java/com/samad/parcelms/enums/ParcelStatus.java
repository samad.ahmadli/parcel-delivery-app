package com.samad.parcelms.enums;

public enum ParcelStatus {

    CREATED,
    ASSIGNED_COURIER,
    IN_PROGRESS,
    DELIVERED,
    CANCELED
}
