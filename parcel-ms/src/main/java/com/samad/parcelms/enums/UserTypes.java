package com.samad.parcelms.enums;

public enum UserTypes {
    USER("ROLE_USER"),
    ADMIN("ROLE_ADMIN"),
    COURIER("ROLE_COURIER");

    private String roleValue;

    UserTypes(String role) {
        this.roleValue = role;
    }

    public static String getRoleValue(UserTypes role) {
        return role.roleValue;
    }
}
