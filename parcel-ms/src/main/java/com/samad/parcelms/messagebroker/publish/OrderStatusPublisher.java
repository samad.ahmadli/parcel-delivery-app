package com.samad.parcelms.messagebroker.publish;

import com.samad.parcelms.config.RabbitMQConfig;
import com.samad.parcelms.messagebroker.model.OrderStatusModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;


@Component
public class OrderStatusPublisher {

    private final Logger log = LoggerFactory.getLogger(OrderStatusPublisher.class);
    private final RabbitTemplate rabbitTemplate;

    public OrderStatusPublisher(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    public void publishParcel(OrderStatusModel orderStatusModel) {

        log.debug("Parcel publish event is started, {} " + orderStatusModel);
        rabbitTemplate.convertAndSend(RabbitMQConfig.ORDER_QUEUE, orderStatusModel);
        log.debug("Parcel publish event is ended, {} " + orderStatusModel);
    }
}
