package com.samad.parcelms.messagebroker.consume;

import com.samad.parcelms.config.RabbitMQConfig;
import com.samad.parcelms.messagebroker.model.ParcelStatusModel;
import com.samad.parcelms.service.ParcelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;


@Component
public class ParcelStatusConsumer {
    private static final Logger log = LoggerFactory.getLogger(ParcelStatusConsumer.class);

    private final ParcelService parcelService;

    public ParcelStatusConsumer(ParcelService parcelService) {
        this.parcelService = parcelService;
    }


    @RabbitListener(queues = RabbitMQConfig.PARCEL_QUEUE)
    public void receiveOrderStatusFromQueue(ParcelStatusModel parcelStatusModel) {

        log.debug("Order status consumed {}" + parcelStatusModel);
        parcelService.addNewParcelOrUpdateStatus(parcelStatusModel);
        log.debug("Order consumed process finished {}" + parcelStatusModel);
    }
}
