package com.samad.parcelms.messagebroker.model;

import com.samad.parcelms.enums.ParcelStatus;

import java.io.Serializable;
import java.math.BigDecimal;


public class ParcelStatusModel implements Serializable {

    private Long orderId;
    private String orderName;
    private String destination;
    private String description;
    private Double weight;
    private BigDecimal price;
    private ParcelStatus status;
    private String customerUsername;
    private String courierUsername;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public ParcelStatus getStatus() {
        return status;
    }

    public void setStatus(ParcelStatus status) {
        this.status = status;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public void setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
    }

    public String getCourierUsername() {
        return courierUsername;
    }

    public void setCourierUsername(String courierUsername) {
        this.courierUsername = courierUsername;
    }

    @Override
    public String toString() {
        return "ParcelStatusModel{" +
                "orderId=" + orderId +
                ", orderName='" + orderName + '\'' +
                ", destination='" + destination + '\'' +
                ", description='" + description + '\'' +
                ", weight=" + weight +
                ", price=" + price +
                ", status=" + status +
                ", customerUsername='" + customerUsername + '\'' +
                ", courierUsername='" + courierUsername + '\'' +
                '}';
    }
}
