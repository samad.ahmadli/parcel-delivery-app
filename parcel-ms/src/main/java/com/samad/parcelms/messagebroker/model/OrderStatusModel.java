package com.samad.parcelms.messagebroker.model;

import com.samad.parcelms.enums.ParcelStatus;

import java.io.Serializable;


public class OrderStatusModel implements Serializable {

    private Long orderId;
    private ParcelStatus status;

    public Long getOrderId() {
        return orderId;
    }

    public OrderStatusModel setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public ParcelStatus getStatus() {
        return status;
    }

    public OrderStatusModel setStatus(ParcelStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        return "OrderStatusModel{" +
                "orderId=" + orderId +
                ", status=" + status +
                '}';
    }
}
