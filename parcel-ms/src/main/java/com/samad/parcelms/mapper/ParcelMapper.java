package com.samad.parcelms.mapper;

import com.samad.parcelms.dto.response.ParcelResponseDTO;
import com.samad.parcelms.entity.Parcel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface ParcelMapper {
    ParcelMapper INSTANCE = Mappers.getMapper(ParcelMapper.class);

    ParcelResponseDTO toDto(Parcel parcel);
}
