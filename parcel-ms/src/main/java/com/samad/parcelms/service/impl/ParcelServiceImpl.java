package com.samad.parcelms.service.impl;

import com.samad.parcelms.config.AuthUserBean;
import com.samad.parcelms.dto.request.ChangeParcelStatusRequestDTO;
import com.samad.parcelms.dto.response.ParcelResponseDTO;
import com.samad.parcelms.entity.Parcel;
import com.samad.parcelms.enums.ParcelStatus;
import com.samad.parcelms.enums.UserTypes;
import com.samad.parcelms.exception.CommonParcelException;
import com.samad.parcelms.mapper.ParcelMapper;
import com.samad.parcelms.messagebroker.model.OrderStatusModel;
import com.samad.parcelms.messagebroker.model.ParcelStatusModel;
import com.samad.parcelms.messagebroker.publish.OrderStatusPublisher;
import com.samad.parcelms.repository.ParcelRepository;
import com.samad.parcelms.service.ParcelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Transactional
public class ParcelServiceImpl implements ParcelService {
    private final AuthUserBean authUserBean;
    private final ParcelRepository parcelRepository;
    private final OrderStatusPublisher orderStatusPublisher;

    public ParcelServiceImpl(AuthUserBean authUserBean, ParcelRepository parcelRepository, OrderStatusPublisher orderStatusPublisher) {
        this.authUserBean = authUserBean;
        this.parcelRepository = parcelRepository;
        this.orderStatusPublisher = orderStatusPublisher;
    }


    @Override
    public List<ParcelResponseDTO> getAllParcels() {
        List<Parcel> allparcels = authUserBean.getRoles().contains(UserTypes.ADMIN.name()) ?
                parcelRepository.findAll() :
                getCourierAllParcels(authUserBean.getUsername());
        return allparcels.stream().map(ParcelMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

    @Override
    public ParcelResponseDTO getParcelDetailsById(Long parcelId) {
        Optional<Parcel> optionalParcel = authUserBean.getRoles().contains(UserTypes.ADMIN.name()) ?
                findParcelById(parcelId) :
                findParcelByIdAndCourier(parcelId, authUserBean.getUsername());
        Parcel parcel = optionalParcel.orElseThrow(() -> new CommonParcelException(400, "Parcel can not found, parcelId:" + parcelId));
        return ParcelMapper.INSTANCE.toDto(parcel);
    }

    @Override
    public void changeParcelStatus(ChangeParcelStatusRequestDTO changeParcelStatusRequest, Long parcelId) {

        Parcel parcel = findParcelByIdAndCourier(parcelId, authUserBean.getUsername())
                .orElseThrow(() -> new CommonParcelException(400, "Parcel can not found, parcelId:" + parcelId));

        if (changeParcelStatusRequest.getParcelStatus().equals(ParcelStatus.IN_PROGRESS)) {
            changeParcelStatusToInProgress(parcel);
        } else if (changeParcelStatusRequest.getParcelStatus().equals(ParcelStatus.DELIVERED)) {
            changeParcelStatusToDelivered(parcel);
        }
    }

    private void changeParcelStatusToInProgress(Parcel parcel) {

        if (!parcel.getStatus().equals(ParcelStatus.ASSIGNED_COURIER)) {
            throw new CommonParcelException(400, "You can't change status of parcel to in-progress, Parcel status: " + parcel.getStatus());
        }

        save(parcel.setStatus(ParcelStatus.IN_PROGRESS).setUpdatedAt(LocalDateTime.now()));
        publishOrderStatusToBroker(parcel.getOrderId(), ParcelStatus.IN_PROGRESS);
    }

    private void changeParcelStatusToDelivered(Parcel parcel) {

        if (!parcel.getStatus().equals(ParcelStatus.IN_PROGRESS)) {
            throw new CommonParcelException(400, "You can't change status of parcel to in-progress, Parcel status: " + parcel.getStatus());
        }

        save(parcel.setStatus(ParcelStatus.DELIVERED).setUpdatedAt(LocalDateTime.now()));
        publishOrderStatusToBroker(parcel.getOrderId(), ParcelStatus.DELIVERED);
    }

    private void publishOrderStatusToBroker(Long orderId, ParcelStatus parcelStatus) {

        OrderStatusModel orderStatusModel =
                new OrderStatusModel().setOrderId(orderId).setStatus(parcelStatus);
        orderStatusPublisher.publishParcel(orderStatusModel);
    }

    @Override
    public void addNewParcelOrUpdateStatus(ParcelStatusModel parcelStatusModel) {
        if (parcelStatusModel.getStatus().equals(ParcelStatus.ASSIGNED_COURIER)) {  // first time create parcel
            Parcel parcel =
                    new Parcel()
                            .setPrice(parcelStatusModel.getPrice())
                            .setStatus(parcelStatusModel.getStatus())
                            .setWeight(parcelStatusModel.getWeight())
                            .setOrderId(parcelStatusModel.getOrderId())
                            .setOrderName(parcelStatusModel.getOrderName())
                            .setDestination(parcelStatusModel.getDestination())
                            .setOrderDescription(parcelStatusModel.getDescription())
                            .setCourierUsername(parcelStatusModel.getCourierUsername())
                            .setCustomerUsername(parcelStatusModel.getCustomerUsername());
            save(parcel);
        } else if (parcelStatusModel.getStatus().equals(ParcelStatus.CANCELED)) {

            Parcel parcel = findParcelByOrderId(parcelStatusModel.getOrderId())
                    .orElseThrow(() -> new CommonParcelException(400, "Parcel can not found by orderId, orderId:" + parcelStatusModel.getOrderId()));
            save(parcel.setStatus(ParcelStatus.CANCELED));
        }
    }

    @Override
    public void save(Parcel parcel) {
        parcelRepository.save(parcel);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Parcel> findParcelByOrderId(Long orderId) {
        return parcelRepository.findByOrderId(orderId);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Parcel> findParcelById(Long parcelId) {
        return parcelRepository.findById(parcelId);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Parcel> findParcelByIdAndCourier(Long parcelId, String courierUsername) {
        return parcelRepository.findByIdAndCourierUsername(parcelId, courierUsername);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Parcel> getCourierAllParcels(String courierUsername) {
        return parcelRepository.getAllByCourierUsername(courierUsername);
    }
}
