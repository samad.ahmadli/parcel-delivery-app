package com.samad.parcelms.service;

public interface AuthService {

    boolean checkUserRoles(String jwtToken, String[] possibleRoles);
}
