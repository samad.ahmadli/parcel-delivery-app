package com.samad.parcelms.service;

import com.samad.parcelms.dto.request.ChangeParcelStatusRequestDTO;
import com.samad.parcelms.dto.response.ParcelResponseDTO;
import com.samad.parcelms.entity.Parcel;
import com.samad.parcelms.messagebroker.model.ParcelStatusModel;
import java.util.List;
import java.util.Optional;


public interface ParcelService {

    List<ParcelResponseDTO> getAllParcels();

    ParcelResponseDTO getParcelDetailsById(Long parcelId);

    void changeParcelStatus(ChangeParcelStatusRequestDTO changeParcelStatusRequest, Long parcelId);

    void addNewParcelOrUpdateStatus(ParcelStatusModel parcelStatusModel);

    void save(Parcel parcel);

    Optional<Parcel> findParcelByOrderId(Long orderId);

    Optional<Parcel> findParcelById(Long parcelId);

    Optional<Parcel> findParcelByIdAndCourier(Long parcelId, String courierUsername);

    List<Parcel> getCourierAllParcels(String courierUsername);
}
