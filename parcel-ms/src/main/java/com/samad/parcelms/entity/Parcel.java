package com.samad.parcelms.entity;

import com.samad.parcelms.enums.ParcelStatus;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Entity
@Table(name = "parcels")
@DynamicInsert
@DynamicUpdate
public class Parcel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "order_id", nullable = false)
    private Long orderId;

    @Column(name = "order_name", nullable = false)
    private String orderName;

    @Column(name = "order_description")
    private String orderDescription;

    @Column(name = "order_weight", nullable = false)
    private Double weight;

    @Column(name = "order_price", nullable = false)
    private BigDecimal price;

    @Column(name = "destination", nullable = false)
    private String destination;

    @Column(name = "coordinates")
    private String coordinates;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private ParcelStatus status;

    @Column(name = "customer_username", nullable = false)
    private String customerUsername;

    @Column(name = "courier_username", nullable = false)
    private String courierUsername;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;


    public Long getId() {
        return id;
    }

    public Parcel setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getOrderId() {
        return orderId;
    }

    public Parcel setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public String getOrderName() {
        return orderName;
    }

    public Parcel setOrderName(String orderName) {
        this.orderName = orderName;
        return this;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public Parcel setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
        return this;
    }

    public Double getWeight() {
        return weight;
    }

    public Parcel setWeight(Double weight) {
        this.weight = weight;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Parcel setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public Parcel setDestination(String destination) {
        this.destination = destination;
        return this;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public Parcel setCoordinates(String coordinates) {
        this.coordinates = coordinates;
        return this;
    }

    public ParcelStatus getStatus() {
        return status;
    }

    public Parcel setStatus(ParcelStatus status) {
        this.status = status;
        return this;
    }

    public String getCustomerUsername() {
        return customerUsername;
    }

    public Parcel setCustomerUsername(String customerUsername) {
        this.customerUsername = customerUsername;
        return this;
    }

    public String getCourierUsername() {
        return courierUsername;
    }

    public Parcel setCourierUsername(String courierUsername) {
        this.courierUsername = courierUsername;
        return this;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Parcel setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Parcel setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
}
