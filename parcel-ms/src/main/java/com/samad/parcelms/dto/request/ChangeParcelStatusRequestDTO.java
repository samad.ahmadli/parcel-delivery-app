package com.samad.parcelms.dto.request;

import com.samad.parcelms.enums.ParcelStatus;
import java.io.Serializable;


public class ChangeParcelStatusRequestDTO implements Serializable {

    private ParcelStatus parcelStatus;

    public ParcelStatus getParcelStatus() {
        return parcelStatus;
    }

    public void setParcelStatus(ParcelStatus parcelStatus) {
        this.parcelStatus = parcelStatus;
    }
}
