package com.samad.parcelms.dto.request;

import java.io.Serializable;


public class CheckJwtTokenRequestDTO implements Serializable {

    private String jwtToken;

    public String getJwtToken() {
        return jwtToken;
    }

    public CheckJwtTokenRequestDTO setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
        return this;
    }
}
