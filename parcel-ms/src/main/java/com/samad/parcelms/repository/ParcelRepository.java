package com.samad.parcelms.repository;

import com.samad.parcelms.entity.Parcel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ParcelRepository extends JpaRepository<Parcel, Long> {

    Optional<Parcel> findByOrderId(Long orderId);

    Optional<Parcel> findById(Long parcelId);

    Optional<Parcel> findByIdAndCourierUsername(Long parcelId, String courierUsername);

    List<Parcel> getAllByCourierUsername(String courierUsername);
}
