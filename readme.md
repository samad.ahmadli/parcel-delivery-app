# Parcel Delivery App

There are four different microservice:
- api-gateway (Api gateway for other microservices)
- auth-ms (Authentication and signup services)
- order-ms (Contains services for orders)
- parcel-ms (Contains parcel delivery services)



## How to run microservices
You just need to run compose file, it generate all images and necessary files.
I also added postman collection for easy testing.

## App architecture


![app-architecture](./App%20architecure.jpeg)
